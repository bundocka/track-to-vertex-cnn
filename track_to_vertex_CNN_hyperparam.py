import sys
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import keras
import sklearn
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.externals import joblib
import sklearn.metrics
sys.path.append("%s/../lib" % os.getcwd())
import util
import algos
import tensorflow as tf
import os
import matplotlib.colors as col
import scipy.interpolate
import pickle

from keras import backend as K
from keras.layers import Dense, Dropout
from keras import regularizers
from keras.models import Sequential
from keras import optimizers

def matchPerf1(event, vx):
    nPV =  sum(event)
    if nPV == 0:
        return False
    if len(vx) == 0:
        return 0., 0.
    nPV_reco = sum(vx)
    effi = float(nPV_reco) / float(nPV)
    purity = float((nPV_reco)) / len(vx)
    return effi, purity

def CNN_architecture(inputFeatures, node, depth):
    cnn = keras.layers.Conv1D(node, 1, activation='relu')(inputFeatures)
    cnn = keras.layers.Dropout(0.1)(cnn)
    if depth>1:
        for i in range(depth-1):
            cnn = keras.layers.Conv1D(node, 1, activation='relu', padding = 'same')(cnn)
            cnn = keras.layers.Dropout(0.1)(cnn)
    return cnn

def CNN(X_CNN, y_CNN, batch_size, title):

    nodes = [8, 16, 32, 64]
    depths = [1, 2, 4, 8]

    for node in nodes:
        for depth in depths:

            model_CNN = Sequential()

            inputFeatures = keras.layers.Input(shape=(250, 10))
            cnn = CNN_architecture(inputFeatures, node, depth)
            predictFraction = keras.layers.Conv1D(1, 1, activation='sigmoid')(cnn)

            model_CNN = keras.models.Model(inputs=[inputFeatures],outputs=[predictFraction])

            def my_loss(y_true, y_pred):
                binary_crossentropy = keras.losses.binary_crossentropy(y_true, y_pred) *tf.sign(inputFeatures[:,:,1])#slice it off the input tensor
                binary_crossentropy = tf.reduce_sum(binary_crossentropy, axis = 1)/tf.reduce_sum(tf.sign(binary_crossentropy), axis = 1)
                return binary_crossentropy

            rmsprop = optimizers.RMSprop(lr=0.001)

            model_CNN.compile(loss=my_loss, optimizer=rmsprop, metrics=['accuracy'])

            y_CNN1 = np.expand_dims(y_CNN, axis=-1)

            testFraction = 0.1
            train_size = int((1.-testFraction) * X_CNN.shape[0])
            Xtrain, Xtest, ytrain, ytest = X_CNN[0:train_size], X_CNN[train_size:], y_CNN1[0:train_size], y_CNN1[train_size:]

            history = model_CNN.fit(Xtrain, ytrain, validation_split=0.33, epochs=20, batch_size=batch_size)

            plt.plot(history.history['val_acc'])
            plt.plot(history.history['acc'])
            plt.title('Model accuracy (' + str(title) +  ', ' + str(node) + ' nodes, ' + str(depth) + ' depth)')
            plt.ylabel('accuracy')
            plt.xlabel('epoch')
            plt.legend(['train', 'test'], loc='lower right')
            plt.savefig('/home/hep/jd918/project/L1_trigger/plots/model_accuracy_'+str(title)+'_'+str(node)+'_'+str(depth)+'.png')
            plt.clf()

            plt.plot(history.history['loss'])
            plt.plot(history.history['val_loss'])
            plt.title('Model loss (' + str(title) +  ', ' + str(node) + ' nodes, ' + str(depth) + ' depth)')
            plt.ylabel('loss')
            plt.xlabel('epoch')
            plt.legend(['train', 'test'], loc='upper right')
            plt.savefig('/home/hep/jd918/project/L1_trigger/plots/model_loss_'+str(title)+'_'+str(node)+'_'+str(depth)+'.png')
            plt.clf()

            probPV = model_CNN.predict(Xtest)

            ytest_flat = ytest.flatten()
            probPV_flat = probPV.flatten()
            Xtest_pt_flat = Xtest[:,:,1].flatten()

            plt.hist(probPV_flat[(ytest_flat==1) & (Xtest_pt_flat>0.0)], range=[0., 1.], bins=50, alpha=0.5, density=True)
            plt.hist(probPV_flat[(ytest_flat==0) & (Xtest_pt_flat>0.0)], range=[0., 1.], bins=50, alpha=0.5, density=True)
            plt.xlabel("predicted weights")
            plt.title('CNN output (' + str(title) +  ', ' + str(node) + ' nodes, ' + str(depth) + ' depth)')
            plt.legend(['PV', 'PU'], loc='upper right')
            plt.savefig('/home/hep/jd918/project/L1_trigger/plots/PVvsPU_'+str(title)+'_'+str(node)+'_'+str(depth)+'.png')
            plt.clf()

            Efficiency, Purity = [], [] # efficiency, purity
            for cut in np.arange(0., 1., 0.02):
                e, p = matchPerf1(ytest_flat[Xtest_pt_flat>0.0], ytest_flat[Xtest_pt_flat>0.0][probPV_flat[Xtest_pt_flat>0.0] > cut])
                Efficiency.append(e)
                Purity.append(p)

            plt.plot(Efficiency, Purity, '.')
            plt.xlabel("Efficiency")
            plt.ylabel("Matching Purity")
            plt.title('Efficiency vs Purity ROC for association (' + str(title) +  ', ' + str(node) + ' nodes, ' + str(depth) + ' depth)')
            plt.savefig('/home/hep/jd918/project/L1_trigger/plots/EffvsPur_'+str(title)+'_'+str(node)+'_'+str(depth)+'.png')
            plt.clf()

            AUC = sklearn.metrics.auc(Efficiency, Purity)

            file = open('/home/hep/jd918/project/L1_trigger/textfiles/Output'+str(title)+'_'+str(node)+'_'+str(depth)+'.txt', "w")
            file.write('AUC of purity vs efficiency: ' + str(AUC))
            file.write("\n")
            auc1 = AUC

            FPR_CNN, TPR_CNN = [], []

            for cut in np.arange(0., 1., 0.001):
                N = len(ytest_flat[Xtest_pt_flat>0.0]) - sum(ytest_flat[Xtest_pt_flat>0.0])
                P = sum(ytest_flat[Xtest_pt_flat>0.0])
                fp = np.sum((probPV_flat[Xtest_pt_flat>0.0] > cut) & (ytest_flat[Xtest_pt_flat>0.0] == 0))
                tp = np.sum((probPV_flat[Xtest_pt_flat>0.0] > cut) & (ytest_flat[Xtest_pt_flat>0.0] == 1))
                if (N != 0):
                    FPR_CNN.append(float(fp)/float(N))
                    TPR_CNN.append(float(tp)/float(P))

            AUC = sklearn.metrics.auc(TPR_CNN, FPR_CNN)
            file.write('AUC for traditional ROC curve: ' + str(AUC))
            auc2 = AUC
            file.write("\n")

            fpr = scipy.interpolate.interp1d(FPR_CNN, TPR_CNN)
            file.write('TPR at which FPR = 1%:  ' + str(fpr(0.01)))
            fpr1 = float(fpr(0.01))
            file.write("\n")

            plt.semilogy(TPR_CNN, FPR_CNN)
            plt.hlines(1e-2, 0, fpr1, linestyle="dashed")
            plt.vlines(fpr1, 1e-4, 1e-2, linestyle="dashed")
            plt.xlabel("True Positive Rate")
            plt.ylabel("False Positive Rate")
            plt.title('TPR vs FPR ROC for association (' + str(title) +  ', ' + str(node) + ' nodes, ' + str(depth) + ' depth)')
            plt.savefig('/home/hep/jd918/project/L1_trigger/plots/FPR_vs_TPR_'+str(title)+'_'+str(node)+'_'+str(depth)+'.png')
            plt.clf()

            file.close()

            dumplings = [auc1, auc2, fpr1]
            fileName = '/home/hep/jd918/project/L1_trigger/textfiles/Output'+str(title)+'_'+str(node)+'_'+str(depth)+'.pkl'
            fileObject = open(fileName, 'wb')
            pickle.dump(dumplings, fileObject)
            pickle.dump(TPR_CNN, fileObject)
            pickle.dump(FPR_CNN, fileObject)
            file.close()
