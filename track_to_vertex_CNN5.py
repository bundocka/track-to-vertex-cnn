import track_to_vertex_CNN
import track_to_vertex_CNN_hyperparam
import track_to_vertex_CNN_hyperparam1
import numpy as np
import h5py
h5pyFile = h5py.File('/home/hep/jd918/project/L1_trigger/data/ttbar-relval-100k-processed-assoc.hdf5', 'r')

trainingData = {}

for k in h5pyFile.keys():
    trainingData[k] = [h5pyFile[k][()]]

for k in trainingData.keys():
    trainingData[k] = np.concatenate(trainingData[k],axis=0)

X_CNN = trainingData['X'][()] # shape (nexamples, ntracks_per_examples, nfeatures_per_track)
y_CNN = trainingData['assoc'][()]
z0_PV = trainingData['y_avg'][()]

for i in range(X_CNN.shape[0]):
    for j in range(X_CNN[i].shape[0]):
        X_CNN[i][j][4] = np.abs(X_CNN[i][j][0] - z0_PV[i])
        X_CNN[i][j][2] = abs(X_CNN[i][j][2])
        if (X_CNN[i][j][1] > 500):
            X_CNN[i][j][1] = 0.0
        if (X_CNN[i][j][1] != 0.0):
            X_CNN[i][j][1] = 1.0/X_CNN[i][j][1]

batch_size = 400
title = 'Conv_pt500_5feat_invpt'

params = {'lr': [0.0001, 0.0003, 0.001, 0.003, 0.01, 0.03, 0.1],
 'node':[32],
 'depth': [2],
 'dropout': [0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4]}#(0, 0.40, 10)}

track_to_vertex_CNN.CNN(X_CNN, y_CNN, batch_size, title)
#track_to_vertex_CNN_hyperparam.CNN(X_CNN, y_CNN, batch_size, title)
#track_to_vertex_CNN_hyperparam1.CNN(X_CNN, y_CNN, batch_size, title, params)
