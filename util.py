''' Utility to read the ntuples of tracks for vertex related studies '''

import uproot
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle

from sklearn.externals import joblib
import statsmodels.api as sm
import os

import random
import algos

# --------------------- utility functions start ------------------- #
def ptToInv2R(pt):
  B = 3.8112 # Tesla
  return abs((B * (3e8 / 1e11)) / (2 * pt))

def tanL(eta):
  '''Obtain tan lambda track fitting parameter from eta'''
  return abs(1. / np.tan(2 * np.arctan(np.exp(-eta))))

def transformEvent(event):
  ''' Put the transformed variables into the event for the BDT '''
  event['inv2R'] = ptToInv2R(event['pt'])
  event['tanL'] = tanL(event['eta'])
  return event

def matchPerf(event, vx):
  """Evaluate the matching performance

  Keyword arguments:
  event -- list of tracks in a given event
  vx    -- list of tracks "matched" to belong with the PV in the same event

  """
  # print("event")
  # print(event)
  # print(vx)
  nPV =  sum(event['fromPV'])
  if nPV == 0:
    return False
  if len(vx) == 0:
    return 0., 0.
  nPV_reco = sum(vx['fromPV'])
  effi = float(nPV_reco) / nPV
  # purity = 1 - (float(len(vx) - nPV_reco) / len(vx))
  purity = float((nPV_reco)) / len(vx)
  return effi, purity

def matchPerfPtWeight(event, vx):
  """Evaluate the matching performance

  Keyword arguments:
  event -- list of tracks in a given event
  vx    -- list of tracks "matched" to belong with the PV in the same event

  """
  # print("event")
  # print(event)
  # print(vx)
  ptPV =  sum(event[event['fromPV']]['pt'])
  if ptPV == 0:
    return False
  if len(vx) == 0:
    return 0., 0.
  ptPV_reco = sum(vx[vx['fromPV']]['pt'])
  effi = float(ptPV_reco) / ptPV
  # purity = 1 - (float(len(vx) - nPV_reco) / len(vx))
  purity = ptPV_recp / sum(vx['pt'])
  return effi, purity

def pvTracks(event):
  ''' Return all tracks associated with the primary vertex '''
  return event[event['fromPV']]

def nonPVTracks(event):
  ''' Return all tracks not associated with the primary vertex '''
  return event[event['fromPV'] == False]

def genuineTracks(event):
  ''' Return all genuine tracks '''
  return event[event['genuine'].astype('bool')]

def fakeTracks(event):
  ''' Return all fake tracks '''
  return event[event['genuine'].astype('bool') == False]

def pvz0(event):
  ''' Return the z0 of the PV. When multiple PVs, return the one with smallest dxy '''
  pvts = pvTracks(event)
  # TODO get the PV from other info if non of the tracks match
  if len(pvts['z0']) == 0:
    return False
  elif len(np.unique(pvts['tpz0'])) == 1:
    return pvts['tpz0'].iloc[0]
  else:
    return pvts['tpz0'].iloc[pvts['tpd0'].values.argmin()]

def pvz0_avg(event):
  ''' Return the z0 of the PV. When multiple PVs, return the one with smallest dxy '''
  pvts = pvTracks(event)
  # TODO get the PV from other info if non of the tracks match
  if len(pvts['z0']) == 0:
    return False
  else:
    return np.mean(pvts['z0'])

def pvd0(event):
  ''' Return the z0 of the PV. When multiple PVs, return the one with smallest dxy '''
  pvts = pvTracks(event)
  # TODO get the PV from other info if non of the tracks match
  if len(pvts['tpd0']) == 0:
    return False
  elif len(np.unique(pvts['tpd0'])) == 1:
    return pvts['tpd0'].iloc[0]
  else:
    return pvts['tpd0'].min()

def plotZ0(event, tracks=True, pv=True, allpvs=False, join=False):
  ''' Plot a z-axis view of the tracks, with optional PV '''
  if tracks:
    plt.plot(nonPVTracks(event)['z0'], [0] * len(nonPVTracks(event)['z0']), '+b', label='PU Trk')
    plt.plot(fakeTracks(event)['z0'], [0] * len(fakeTracks(event)['z0']), '+r', label='Fake Trk')
    plt.plot(pvTracks(event)['z0'], [0] * len(pvTracks(event)['z0']), '+g', label='PV Trk')
  if pv:
    if pvz0:
      plt.plot(pvz0(event), pvd0(event))
  if allpvs:
    pv_zs = event['tpz0'][event['fromPV']]
    pv_ds = event['tpd0'][event['fromPV']]
    plt.plot(pv_zs, pv_ds, '+k')
    #plt.ylim((-1, 1))
  # join the primary vertex tracks to that vertex
  if join:
    pvts = pvTracks(event)
    for i in range(len(pvts['z0'])):
      plt.plot([pvts['z0'][i], pvts['tpz0'][i]], [0, pvts['tpd0'][i]], '--k')

def plotKDEandTracks(event, vx_trks=None, d = None, bw=0.2, weight_field=None, kernel='uni'):
  if d is None:
    d = algos.kde(event, bw=bw, weight_field=weight_field, kernel=kernel)
  plt.fill(d[0], d[1])
  plotZ0(event, pv=True)
  vx = algos.kdeVX(event, d=d)
  plt.plot([vx, vx], [0, max(d[1])], '--k', label='Reco Vx')
  pvz = pvz0(event)
  plt.plot([pvz, pvz], [0,max(d[1])], '--g', label='True Vx')
  #if vx_trks is None:
  #  vx_trks = kdeVXTrks(event, d=d)
  # show a dashed line just below the axis covering the tracks reco'd to the PV
  #plt.plot([min(vx_trks['z0']), max(vx_trks['z0'])], [-0.1, -0.1], '--k')
  plt.xlabel('z / cm')
  plt.ylabel('density')
  plt.legend()
  #match_e, match_c = matchPerf(event, vx_trks)
  #plt.title('z_reco - z_true = ' + "%.2f" % (vx - pvz) + ', match_eff = ' + str(match_e) + ', contamination = ' + str(match_c))

def plotRandomEvent(bw=0.2, weight_field=None, kernel='uni', density_threshold=0.01):
  iEv = random.randint(0, 8800)
  d = algos.kde(events[iEv], bw=bw, weight_field=weight_field, kernel=kernel)
  vx_trks = kdeVXTrks(events[iEv],  d=d, density_threshold=density_threshold)
  plotKDEandTracks(events[iEv], d=d, vx_trks=vx_trks)
  return iEv


def loadData(filename, use_bdt = False):
  """

  Args:
    filename (string):
    use_bdt  (bool):
  """
  DeprecationWarning("Please use util.loadDataSingleFile()")
  return loadDataSingleFile(filename, use_bdt)

def loadDataSingleFile(filename, use_bdt = False):
  """
  New method for loading training events. It assumes that only one root file is available (as a result of hadd command)
  """

  # check if filename links to a valid file
  if (not os.path.isfile(filename)):
    raise FileNotFoundError("Trying to load data from a file that does not exist: %s" % filename)

  noevts = len(uproot.open(filename)['L1TrackNtuple_TMTT_KF4ParamsComb/eventTree'])

  features = ['inv2R', 'tanL', 'chi2', 'nSkipped']
  branches = ['trk_z0', 'trk_pt', 'trk_fromPV', 'trk_genuine', 'trk_matchtp_z0', 'trk_matchtp_pt', 'trk_matchtp_eta', 'trk_matchtp_dxy','trk_nSkippedLayers', 'trk_chi2', 'trk_eta', 'trk_matchtp_pdgid']
  events = [{}] * noevts

  long_to_short = {'trk_z0' : 'z0', 'trk_pt' : 'pt', 'trk_fromPV' : 'fromPV',
                   'trk_genuine' : 'genuine', 'trk_matchtp_z0' : 'tpz0','trk_matchtp_pt' : 'tppt', 'trk_matchtp_eta' : 'tpeta', 'trk_matchtp_dxy' : 'tpd0',
                 'trk_chi2' : 'chi2', 'trk_nSkippedLayers' : 'nSkipped', 'trk_eta' : 'eta', 'trk_matchtp_pdgid':'pdgid'}

  # Read the relevant branches into a list of events
  events = {}
  for branch in branches:
    events[long_to_short[branch]] = []

  # for i in range(1, 89):
  data = uproot.open(filename)["L1TrackNtuple_TMTT_KF4ParamsComb"]["eventTree"].arrays(branches)
  for branch in data:
    for event in data[branch]:
      events[long_to_short[branch.decode("utf-8")]].append(event)

  if (use_bdt):
    bdt = joblib.load('../bdt.pkl')

  # Pivot from dict of lists of arrays to list of dicts of arrays
  x = []
  for i in range(noevts):
    y = {}
    for branch in branches:
      y[long_to_short[branch]] = events[long_to_short[branch]][i]
    x.append(pd.DataFrame(transformEvent(y)))
  events = x

  # Add some fields used to weight tracks
  for event in events:
    # vtxz0 = pvz0(event)
    # TODO: Consider reconstructing the position of each event vertex all at once (to minimise code executed in loop)
    vtxz0fs = algos.fastHistoVX(event, weight_field='pt')
    if (use_bdt):
      event['prob_gen'] = bdt.predict_proba(event[features])[:,1]
      event['bdt_pt2'] = event['prob_gen'] * event['pt']**2
    event['pt2'] = event['pt']**2
    # event['dz'] = event['z0'] - vtxz0 # this can be used when training with reco z0 being the generator position
    event['dz'] = event['z0'] - vtxz0fs

  return events

def prepare_training_data(
    root_filename = "ttbar-100k.root",
    nbins = 256,
    hrange=(-15, 15),
    export_filename="ttbar100k256b_pt.pkl",
    export_events = False,
    overwrite = False,
    load_if_available = True,
):

  X = []
  y = []
  events = []

  if (load_if_available and os.path.isfile(export_filename)):
    with open(export_filename, "rb") as f:
      (X, y, events) = pickle.load(f)
  else:
    events = loadDataSingleFile(fname="ttbar-100k.root")

    for e in events:
      vals, bins = np.histogram(e.z0, weights=e['pt'], bins=256, range=(-15, 15));
      X.append(vals)
      y.append(pvz0(e))

    X = np.array(X)
    y = np.array(y)

    save_pkl = overwrite or (not os.path.isfile(export_filename))
    if (save_pkl):
      if (export_events):
        data = (X, y, events)
      else:
        data = (X, y)
      with open(export_filename, "wb") as f:
          pickle.dump(data, f)

  if (not export_events):
    events = []

  return (X, y, events)

def train_assoc_bdt(events, use_bdt = False):

  bdt_training_columns = ['chi2', 'eta', 'fromPV', 'inv2R', 'nSkipped', 'pt', 'tanL', 'z0', 'dz']
  if (use_bdt):
    bdt_training_columns.append('prob_gen')
  events_merged = pd.concat(events)
  # events_merged[bdt_training_columns].to_csv('events_assignment_dataset.csv')

  # can export dataset here
  # events.to_csv("../data/ttbar-events-pd-df.csv", sep='\t')

  # training bdt (should go to separate file)
  print("Training...")
  from sklearn.ensemble import GradientBoostingClassifier
  from sklearn.model_selection import train_test_split
  x_labels = ['chi2', 'eta', 'inv2R', 'nSkipped', 'pt', 'tanL', 'z0', 'dz']
  if (use_bdt):
    x_labels.append("prob_gen")
  y_labels = ['fromPV']
  X = events_merged[x_labels]
  y = events_merged[y_labels]

  X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
  # check if assoc.pkl exists and train model only if not, otherwise load the association info from pkl file

  # load = True
  load = os.path.isfile("../bdt/assoc.pkl")

  if (load):
    clf = joblib.load('../bdt/assoc.pkl')
  else:
    clf = GradientBoostingClassifier(verbose=1).fit(X_train, y_train)
    joblib.dump(clf, '../bdt/assoc.pkl')
    # joblib.dump(clf, '../bdt/assoc.joblib')
    # pickle.dump( favorite_color, open( "../bdt/assoc.pkl", "wb" ) )

  print("Analysing feature significance...")
  importances = clf.feature_importances_
  for key, value in enumerate(x_labels):
    print("%s:\t%05f" % (value, importances[key]))

  feature_importance = clf.feature_importances_
  # feature_importance = (feature_importance / feature_importance.max())
  sorted_idx = np.argsort(feature_importance)
  pos = np.arange(sorted_idx.shape[0]) + .5
  print(feature_importance)
  plt.barh(pos, feature_importance[sorted_idx], align='center')
  plt.yticks(pos, np.array(x_labels)[sorted_idx])
  plt.xlabel('Relative Importance')
  plt.title('Variable Importance')
  plt.savefig("vx_bdt_features.png")
  plt.close()
  plt.show()
  # print(importances)
  # std = np.std([tree.feature_importances_ for tree in clf.estimators_],
  #              axis=0)
  # indices = np.argsort(importances)[::-1]

  # # Print the feature ranking
  # print("Feature ranking:")

  # for f in range(X.shape[1]):
  #     print("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))

  print("Association BDT score: %f" % clf.score(X_test, y_test))

  # check if pkl file exists
  # joblib.dump(clf, 'assoc.pkl')

  # needs an import
  # from sklearn.metrics import roc_curve
  # fpr, tpr, thres = roc_curve(y_test, clf.score(X_test, y_test))
  # plt.figure()
  # plt.plot(fpr, tpr)
  # plt.show()

  print("generating association BDT ROC...")
  # validation
  y_score = clf.predict_proba(X_test)

  eff_list = []
  cont_list = []
  for thresh_i in range(0, 99):
    thresh = thresh_i / 100.0
    eff, cont = matchPerf(y_test, y_test[y_score[:,1] > thresh])
    eff_list.append(eff)
    cont_list.append(cont)

  y_score_am = algos.fastHistoMatchingPerTracks(X_test, adaptive=True)

  print("generating adaptive matching association ROC...")
  eff_list_am = []
  cont_list_am = []
  eff_list_cm = []
  cont_list_cm = []
  for thresh_i in range(1, 1500):
    thresh = thresh_i / 100.0
    y_score_am = algos.fastHistoMatchingPerTracks(X_test, adaptive=True, res=thresh)
    y_score_cm = algos.fastHistoMatchingPerTracks(X_test, adaptive=False, res=thresh)
    # print(len(y_score_am), len(y_test))
    eff, purity = matchPerf(y_test, y_test[y_score_am == True])
    eff_cm, purity_cm = matchPerf(y_test, y_test[y_score_cm == True])
    eff_list_am.append(eff)
    eff_list_cm.append(eff_cm)
    cont_list_am.append(purity)
    cont_list_cm.append(purity_cm)

  y_score_def = algos.fastHistoMatchingPerTracks(X_test, adaptive=True, res=0.33)
  def_match_eff, def_purity = matchPerf(y_test, y_test[y_score_def == True])

  plt.figure()
  plt.title("Track to vertex assignment BDT ROC")
  plt.xlabel("Matching efficiency")
  plt.ylabel("Purity")
  plt.plot(eff_list, cont_list)
  plt.savefig("../plots/vx_bdt_association_roc.png")
  plt.close()

  plt.figure()
  plt.title("Track to vertex assignment AM ROC")
  plt.xlabel("Matching efficiency")
  plt.ylabel("Purity")
  plt.plot(eff_list_am, cont_list_am)
  plt.savefig("../plots/vx_am_association_roc.png")
  plt.close()

  plt.figure()
  plt.title("Track to vertex assignment ROC")
  plt.xlabel("Matching efficiency")
  plt.ylabel("Purity")
  plt.plot(eff_list, cont_list, label="BDT")
  plt.plot(eff_list_am, cont_list_am, '.', label="Adaptive Matching")
  plt.plot(eff_list_cm, cont_list_cm, label="FastHisto Matching (fixed window)")
  plt.plot([def_match_eff], [def_purity], 'rx', label='Adaptive Matching Default')
  plt.legend()
  plt.savefig("../plots/vx_association_roc.pdf")
  plt.show()

  return

# --------------------- utility functions end ------------------- #
