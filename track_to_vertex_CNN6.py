import track_to_vertex_CNN
import track_to_vertex_CNN_hyperparam
import numpy as np
import h5py

h5pyFile = h5py.File('/home/hep/jd918/project/L1_trigger/data/zmm-relval-100k-processed.hdf5', 'r')
#h5pyFile = h5py.File('/home/hep/jd918/project/L1_trigger/data/ttbar-relval-100k-processed-assoc.hdf5', 'r')
#h5pyFile = h5py.File('/home/hep/jd918/project/L1_trigger/data/ttbar-relval-9k-processed-assoc.hdf5', 'r')

trainingData = {}

for k in h5pyFile.keys():
    trainingData[k] = [h5pyFile[k][()]]

for k in trainingData.keys():
    trainingData[k] = np.concatenate(trainingData[k],axis=0)

X_CNN = trainingData['X'][()] # shape (nexamples, ntracks_per_examples, nfeatures_per_track)
y_CNN = trainingData['assoc'][()]
z0_PV = trainingData['y_avg'][()]

for i in range(X_CNN.shape[0]):
    for j in range(X_CNN[i].shape[0]):
        X_CNN[i][j][4] = X_CNN[i][j][0] - z0_PV[i]

batch_size = 400
title = 'Conv_basic_Zmumu'

track_to_vertex_CNN.CNN(X_CNN, y_CNN, batch_size, title)
#track_to_vertex_CNN_hyperparam.CNN(X_CNN, y_CNN, batch_size, title)
